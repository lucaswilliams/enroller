<?php
    $org = $_GET['org'];
    
    $_WSTOKEN = '00DAD897-4E82-4C3D-92D5B0A0050E7603';
    $_APITOKEN = '986B5C41-48E8-4D11-A68D0A8228FC3336';

    $ax_url = 'https://admin.axcelerate.com.au/api/';
    if($_SERVER['SERVER_NAME'] == 'localhost') {
        $ax_url = 'https://stg.axcelerate.com.au/api/';
    }
    $service_url = $ax_url.'organisations?displayLength=50&name='.urlencode($org);
    $headers = array(
        'wstoken: ' . $_WSTOKEN,
        'apitoken: ' . $_APITOKEN
    );
    
    $curl = curl_init($service_url);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
    
    $curl_response = curl_exec($curl);
    $data = json_decode($curl_response);
    
    $results = array();
    foreach($data as $item) {
        $results[] = array(
            'id' => $item->ORGID,
            'text' => $item->NAME
        );
    }
    
    echo json_encode($results);