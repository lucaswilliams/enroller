<?php
    session_start();
?>
<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
    <title>Real Response Enroller</title>
    <style>
        body {
            padding-bottom: 64px;
        }
        
        nav a, nav img {
            display: block;
            width: 48px;
            height: 48px;
            margin: 5px auto;
        }
        
        .col-6, .col-12 {
            box-shadow: 2px 4px 5px 0 rgba(0,0,0,0.35);
            border-top: 1px solid #F0F0F0;
            margin-top: 64px;
            padding-top: 15px;
            padding-bottom: 15px;
        }
        
        input[type=file] {
            display: block;
        }
    </style>
</head>
<body>

<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a href="logout.php"><img src="img/realresponse.png"></a>
</nav>
<div class="container">
    <div class="row justify-content-center">
        <?php
            //Tokens.  Should be safer elsewhere?
            $_WSTOKEN = '00DAD897-4E82-4C3D-92D5B0A0050E7603';
            $_APITOKEN = '986B5C41-48E8-4D11-A68D0A8228FC3336';
            
            $ax_url = 'https://admin.axcelerate.com.au/api/';
            if($_SERVER['SERVER_NAME'] == 'localhost') {
                $ax_url = 'https://stg.axcelerate.com.au/api/';
            }
            
            if(isset($_POST['username']) && isset($_POST['password'])) {
                if(($_POST['username'] == 'realresponse') && ($_POST['password'] == 'Enr0lmentForm!')) {
                    $_SESSION['loggedin'] = 1;
                }
            }
            
            if($_SESSION == null) {
        ?>
            <div class="col-6">
                <form method="POST">
                    <div class="form-group">
                        <label for="username">Username</label>
                        <input type="text" name="username" id="username" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="password">Password</label>
                        <input type="password" name="password" id="password" class="form-control">
                    </div>
                    <button class="btn btn-danger">Log in</button>
                </form>
            </div>
        <?php
            } elseif($_FILES == null) {
        ?>
            <div class="col-6">
                <form method="POST" enctype="multipart/form-data">
                    <div class="form-group">
                        <label for="organisation">Organisation</label>
                        <select name="organisation" id="organisation" class="select2 form-control">
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="uploadfile">Excel File</label>
                        <input type="file" name="uploadfile" id="uploadfile">
                    </div>
                    <button class="btn btn-danger">Upload</button>
                </form>
            </div>
        <?php
            } else {
                echo '<div class="col-12">';
                $target_dir = "upload".DIRECTORY_SEPARATOR;
                $target_file = $target_dir . basename($_FILES["uploadfile"]["name"]);
                move_uploaded_file($_FILES['uploadfile']['tmp_name'], $target_file);
                require 'SpreadsheetReader.php';
                
                $Reader = new SpreadsheetReader((__DIR__).DIRECTORY_SEPARATOR.$target_file);
                $Sheets = $Reader->Sheets();
    
                foreach ($Sheets as $Index => $Name)
                {
                    $Reader -> ChangeSheet($Index);
                    $i = 0;
                    foreach ($Reader as $Row)
                    {
                        if($i == 0) {
                            $i++;
                        } else {
                            //echo '<pre>'; var_dump($Row); echo '</pre>';
                            $usi = substr($Row[4], 0, 10);
                            $email = '';
                            $given = $Row[0];
                            $middle = $Row[1];
                            $surname = $Row[2];
                            $dob = $Row[3];
                            $email = $Row[5];
    
                            //echo 'EMAIL BEFORE '; var_dump($email);
                            
                            if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                                $email = '';
                            }
    
                            //echo 'EMAIL AFTER '; var_dump($email);
                            
                            if(strlen($dob) > 0) {
                                $day = substr($dob, 3, 2);
                                $month = substr($dob, 0, 2);
                                $year = substr($dob, 6, 2);
    
                                if ((int)'20' . $year > (int)date('Y')) {
                                    $year = '19' . $year;
                                } else {
                                    $year = '20' . $year;
                                }
    
                                $dob = $year . '-' . $month . '-' . $day;
                            }
                            
                            if (strlen($given) > 0 && strlen($surname) > 0) {
                                if(strlen($usi) > 0 || strlen($dob) > 0) {
                                    //Can we find them and update their USI and DOB?
                                    $service_url = $ax_url . 'contacts/search?givenName=' . urlencode($given) . '&surname=' . urlencode($surname);
                                    $headers = array(
                                        'wstoken: ' . $_WSTOKEN,
                                        'apitoken: ' . $_APITOKEN
                                    );

                                    $curl = curl_init($service_url);
                                    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
                                    curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
                                    if ($_SERVER['SERVER_NAME'] == 'localhost') {
                                        curl_setopt($curl, CURLOPT_PROXY, '127.0.0.1:8888');
                                        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
                                        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
                                    }

                                    $curl_response = curl_exec($curl);
                                    $data = json_decode($curl_response);
                                    //var_dump($data);
                                    //Look to see if they have the organisation
                                    $contact_id = 0;
                                    foreach ($data as $item) {
                                        if ($item->ORGANISATION == $_POST['organisation']) {
                                            $contact_id = $item->CONTACTID;
                                        }
                                    }

                                    //echo $contact_id.'<br >';
                                    if ($contact_id > 0) {
                                        //Update the USI
                                        $data = array();
                                        if(strlen($usi) > 0) {
                                            $data["USI"] = $usi;
                                        }
                                        if(strlen($dob) > 0) {
                                            $data["dob"] = $dob;
                                        }
                                        //var_dump($data);
                                        $service_url = $ax_url . 'contact/' . $contact_id;
                                        //echo $service_url.'<br>';
                                        $curl = curl_init($service_url);
                                        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
                                        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
                                        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PUT");
                                        curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($data));
                                        if ($_SERVER['SERVER_NAME'] == 'localhost') {
                                            curl_setopt($curl, CURLOPT_PROXY, '127.0.0.1:8888');
                                            curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
                                            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
                                        }
    
                                        $response = curl_exec($curl);
                                        $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
                                        //var_dump($httpcode);
                                        if(substr($httpcode, 0, 1) == "4") {
                                            $data = json_decode($response);
                                            echo '<p style="color: #FF0000; font-weight: bold;">' . $given . ' ' . $surname . ' was not updated:<br>'.$data->DETAILS.'</p>';
                                        } else {
                                            echo '<p>' . $given . ' ' . $surname . ' updated</p>';
                                        }
                                    } else {
                                        $service_url = $ax_url.'contact';
        
                                        $headers = array(
                                            'wstoken: ' . $_WSTOKEN,
                                            'apitoken: ' . $_APITOKEN
                                        );
                
                                        $params = array(
                                            'givenName' => $given,
                                            'middleName' => $middle,
                                            'surname' => $surname,
                                            'USI' => $usi,
                                            'organisation' => $_POST['organisation']
                                        );
                                        
                                        if(strlen($email) > 0) {
                                            $params['emailAddress'] = $email;
                                        }
                            
                                        if(strlen($dob) > 0) {
                                            $params['dob'] = $dob;
                                        }
                
                                        $fieldsstring = '';
                                        foreach ($params as $key => $value) {
                                            $fieldsstring .= $key . '=' . $value . '&';
                                        }
                                        rtrim($fieldsstring, '&');
                
                                        $curl = curl_init($service_url);
                                        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
                                        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
                                        curl_setopt($curl, CURLOPT_POST, true);
                                        curl_setopt($curl, CURLOPT_POSTFIELDS, $fieldsstring);
                                        if($_SERVER['SERVER_NAME'] == 'localhost') {
                                            curl_setopt($curl, CURLOPT_PROXY, '127.0.0.1:8888');
                                            curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
                                            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
                                        }
                
                
                                        $curl_response = curl_exec($curl);
                                        $data = json_decode($curl_response);
                                        $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
                                        
                                        if(substr($httpcode, 0, 1) == "4") {
                                            echo '<p style="color: #FF0000; font-weight: bold;">' . $given . ' ' . $surname . ' was not created:<br>'.$data->DETAILS.'</p>';
                                        } else {
                                            echo '<p>' . $given . ' ' . $surname . ' created</p>';
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                
                echo '</div>';
            }
        ?>
    </div>
</div>


<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<script>
    $(document).ready(function() {
        $('.select2').select2({
            minimumInputLength: 3,
            ajax: {
                url: function(params) {
                    return 'org.php?org=' + params.term;
                },
                dataType: 'json',
                delay: 500,
                processResults: function (data) {
                    return {
                        results: $.map(data, function (item) {
                            return {
                                text: item.text,
                                id: item.text
                            }
                        })
                    };
                }
            }
        });
    });
</script>
</body>
</html>